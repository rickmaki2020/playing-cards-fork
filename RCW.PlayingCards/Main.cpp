
// Playing Cards
// Part 1: Riley Wuest
// Part 2: Rick Maki

#include <iostream>
#include <conio.h>
#include <string>


using namespace std;

enum Rank { Two = 2, Three, Four, Five, Six, Seven, Eight, Nine, Ten, Jack, Queen, King, Ace };

enum Suit { Diamonds, Hearts, Clubs, Spades };



struct Card
{
	Suit suit;
	Rank rank;

};


void PrintCard(Card card)
{
	
	switch (card.rank)
	{
	case 14: cout << "The Ace of "; break;
	case 13: cout << "The King of "; break;
	case 12: cout << "The Queen of "; break;
	case 11: cout << "The Jack of "; break;
	case 10: cout << "The Ten of "; break;
	case 9: cout << "The Nine of "; break;
	case 8: cout << "The Eight of "; break;
	case 7: cout << "The Seven of "; break;
	case 6: cout << "The Six of "; break;
	case 5: cout << "The Five of "; break;
	case 4: cout << "The Four of "; break;
	case 3: cout << "The Three of "; break;
	case 2: cout << "The Two of "; break;
	}

	switch (card.suit)
	{
	case 3: cout << "Spades"; break;
	case 2: cout << "Clubs"; break;
	case 1: cout << "Hearts"; break;
	case 0: cout << "Diamonds"; break;
	}
}

Card HighCard(Card card1, Card card2)
{
	if (card1.rank > card2.rank)
	{
		return card1;
	}
	else if (card2.rank > card1.rank)
	{
		return card2;
	}
	else if (card1.suit > card2.suit)
	{
		return card1;
	}
	else
	{
		return card2;
	}

}

int main()
{
	Card aos;
	aos.rank = Ace;
	aos.suit = Spades;
	PrintCard(aos);


	(void)_getch();
	return 0;
}


